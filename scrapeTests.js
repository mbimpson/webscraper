var scraper = require('./scraper.js'),
    should = require('should'),
    cheerio = require('cheerio'),
    url = 'http://www.bbc.co.uk/news',
    articleUrl = 'http://www.bbc.co.uk/news/world-us-canada-38277072';

describe('GetArticleHeaders', function() {
    it('check we can select the article headers', function(done) {
        scraper.getPage(url, function(body, response) {
            response.statusCode.should.equal(200);
            var $ = cheerio.load(body),
                headers = $('.gs-c-promo-heading');

            headers.length.should.not.equal(0);
            done();
        });
    });
});

describe('GetArticleContent', function() {
    it('check we can select content from the article', function(done) {
        scraper.getPage(articleUrl, function(body, response) {
            response.statusCode.should.equal(200);
            var $ = cheerio.load(body),
                headers = $('.story-body__introduction');

            headers.length.should.not.equal(0);
            done();
        });
    });
});

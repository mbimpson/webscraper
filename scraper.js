var cheerio = require('cheerio'),
    mainPageUrl = 'https://www.bbc.com/news',
    resultObj = {
        "results": [
        ]
    },
    request = require('request'),
    Rx = require("rxjs");

getPage = (url) => {
    return Rx.Observable.create(observer => {
        request(url, (error, response, body) => {
            if (error) {
                console.log(error);
                observer.error(error);
            }

            observer.next(body);
            observer.complete();
        });
    }, error => {
        console.log(error);
    });
}

parseArticle = (articleUrl) => {
    return Rx.Observable.create(observer => {
        var getPageObservable = getPage(articleUrl);
        getPageObservable.subscribe((body) => {
            var $ = cheerio.load(body),
                title = "";

            title = $('.story-body__introduction').text();
            var result = {
                "url": articleUrl,
                "title": title
            };

            resultObj['results'].push(result);
            observer.next();
            observer.complete();
        }, error => {
            console.log(error);
        });
    });
}

parseMainPageData = (url) => {
    var articleObservables = [];
    var observable = getPage(url);
    observable.subscribe((body) => {
        var $ = cheerio.load(body);
        var max = 0;
        $('.gs-c-promo-heading').each((i, elem) => {
            if (max <= 10) {
                var article =  $(elem).attr('href');
                if (article.startsWith('/news')) {
                    var articleUrl = 'https://www.bbc.com' + article;
                    // Now scrape each article page and parse
                    articleObservables.push(parseArticle(articleUrl));
                }
            }

            max++;
        });

        Rx.forkJoin(articleObservables).subscribe((response) => {
            console.log('done');
            console.log(resultObj);
        });
    });
}

init = () => {
    console.log('Starting web scraper!');
    parseMainPageData(mainPageUrl);
}

init();

module.exports.getPage = getPage;

## Webscraper

## Installation
- Open git bash in the project root
- Run "npm install"

## Usage
- Run "node scraper.js" to run web scraper
- Run "mocha scrapeTests.js" for unit tests
